#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of Bracky, a private guild bot for DCA
# Copyright (C) 2019-2020  Tmpod

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
This is a Discord bot made for the Discord Coding Academy guild.
"""

import logging
import sys

import hikari
import lightbulb

from bracky import __version__
from bracky.config import get_config
from bracky.resources import get_resource

try:
    config_file_path = sys.argv[1]
except IndexError:
    config_file_path = "config.yml"

config = get_config(config_file_path)

# PyCharm is being a bit stupid with this, so I'm supressing this error
# noinspection PyArgumentList
logging.basicConfig(
    level=config.logging.level,
    style="{",
    format=config.logging.log_format,
    datefmt=config.logging.date_format,
)

_LOGGER = logging.getLogger("bracky")

# Read, format and print start banner.
with get_resource("start_banner.txt") as fp:
    raw_text = fp.read()

formatted_text = raw_text.format(
    VERSION=__version__,
    HIKARI_VERSION=hikari.__version__,
    LIGHTBULB_VERSION=lightbulb.__version__,
)
print(formatted_text)

# Import has to be here because Bracky depends on the configuration.
# We should refactor this __main__ logic to fix this.
from bracky.bot import Bracky

# Actually start everything
_LOGGER.info("Starting bot")
bot = Bracky()
bot.run()
