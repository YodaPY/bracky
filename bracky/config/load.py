#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of Bracky, a private guild bot for DCA
# Copyright (C) 2019-2020  Tmpod

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
cattrs powered methods to load the YAML configuration file into data models.
"""
__all__ = [
    "load_config_file",
    "process_raw_config",
    "get_config",
    "CONFIG_CONVERTER",
]

import datetime
import logging
import typing

import cattr
import yaml

from bracky.config.models import Config


_LOGGER = logging.getLogger("bracky.config")


# Converter to use when loading configuration stuff, as not to pollute the global converter, so to speak.
CONFIG_CONVERTER = cattr.Converter()

# Set some useful hooks
# TODO: Add embed hook when Hikari adds a nice from_dict/deserialize method to them.
CONFIG_CONVERTER.register_structure_hook(datetime.datetime, lambda d, _: datetime.datetime.utcfromtimestamp(d))

# Internal property for caching config. Makes the module act as a kind of singleton.
# Not entirely sure if this is a great idea or how to do this better, but this way we can access the configuration
# from anywhere, without having to worry to pass the bot object around.
_config_cache: typing.Optional[Config] = None


def load_config_file(config_file_path: str) -> typing.Dict:
    """
    Safely loads the given config file with PyYAML.
    Raises an exception if the file isn't found, naturally.
    """
    _LOGGER.debug("Loading raw config file")
    # Let it error
    with open(config_file_path) as fp:
        return yaml.safe_load(fp)


def process_raw_config(raw_config_data: typing.Dict) -> Config:
    """
    Processes the raw configuration mapping to the structured data models with cattrs.
    """
    _LOGGER.debug("Populating config models from raw data")
    return Config.from_dict(raw_config_data, CONFIG_CONVERTER)


def get_config(config_file_path: typing.Optional[str] = None) -> Config:
    """
    Returns the cached configuration and attempts to load a new one in case the cache is empty.
    Passing a file path will always lead to the cache being overwritten with a new configuration.
    If the cache is None and no file path is given, an exception will be raised.
    """
    global _config_cache  # eh :/

    if config_file_path is None:
        if _config_cache is None:
            raise ValueError("Cache is empty and no file path was passed!")

        return _config_cache

    _LOGGER.info("Making new config objects")
    # Get a new configuration object.
    config = process_raw_config(load_config_file(config_file_path))
    # And cache it.
    _config_cache = config

    return config
