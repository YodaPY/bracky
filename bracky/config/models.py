#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of Bracky, a private guild bot for DCA
# Copyright (C) 2019-2020  Tmpod

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
Models for our configuration, using attrs and then later on (un)marshalled with cattrs.
"""

__all__ = [
    "BaseConfigModel",
    "BotConfig",
    "LoggingConfig",
    "PostgresConfig",
    "Constants",
    "WelcomerConfig",
    "BotManagerConfig",
    "RoleMenuConfig",
    "Config",
]

import os
from typing import TypeVar, Type, List, Optional, Dict, Any

import attr
import cattr

# Even tho I generally don't like importing classes directly, I'm softer about it when it comes to typing because
# it provides more readability to a feature that is almost at syntax-level, which is nice.


## Base and utility stuff

ThisImplT = TypeVar("ThisImplT")

# Little alias to make the models cleaner to look at, as they are the main reference for when writing the config file.
config = attr.s(frozen=True, auto_attribs=True)


class BaseConfigModel:
    """Basic (un)marshalling functionality, with cattrs."""

    @classmethod
    def from_dict(
        cls: Type[ThisImplT], obj: Dict[str, Any], converter: cattr.Converter = cattr.global_converter
    ) -> ThisImplT:
        return converter.structure(obj, cls)

    def to_dict(self):
        assert attr.has(self.__class__)
        # I have to ignore this error in PyCharm cus it doesn't aknowledge the assert above
        # noinspection PyDataclass
        return attr.asdict(self)


## Actual models start here


@config
class BotConfig(BaseConfigModel):
    """Values related to the actual Discord interface."""

    token: str = attr.ib(repr=False)
    command_prefix: str = "!"
    case_insensitive: bool = True
    plugin_blacklist: List[str] = attr.ib(factory=list)
    additional_plugins: List[str] = attr.ib(factory=list)


@config
class LoggingConfig(BaseConfigModel):
    """Configuration values for the root logger (set via the basicConfig function)."""

    level: str = "INFO"
    log_format: str = "{asctime}.{msecs:03.0f} | {levelname:^8} | {name} :: {message}"
    date_format: str = "%Y-%m-%d %H:%M:%S"


@config
class PostgresConfig(BaseConfigModel):
    """
    All values needed to connect to a PostgreSQL.
    The defaults will let the bot connect to the db service if you're running with Docker.
    """

    password: str = attr.ib(repr=False, factory=lambda: os.environ["POSTGRES_PASSWORD"])
    host: str = "bracky-db"
    port: int = 5432
    user: str = "postgres"
    database: str = "postgres"


@config
class Constants(BaseConfigModel):
    """Miscellaneous assortment of constants, generally IDs."""

    guild: int
    staff_role: int
    bracky_dev_role: int
    bracky_logs_channel: int


@config
class WelcomerConfig(BaseConfigModel):
    """
    Configurations for the welcomer/fareweller component.

    Currently, user is the only valid substitution for messages.
    It will be formatted with a hikari.User object so you
    may use any attributes that are present.

    Example:
        - "`{user.username}{user.discriminator}` just left the guild."
    """

    channel_id: int
    join_messages: List[str] = attr.ib(factory=list)
    leave_messages: List[str] = attr.ib(factory=list)


@config
class BotManagerConfig(BaseConfigModel):
    """
    All the configuration regarding the bot management system.
    At the moment, it just consists of a bunch of Discord IDs for channels and roles required.
    """

    invite_channel: int
    invite_log_channel: int
    bots_role: int
    bot_developers_role: int


@config
class RoleMenuConfig(BaseConfigModel):
    """Configuration values for a role menu."""

    channel: int
    message: int
    # Emoji -> Role ID
    roles: Dict[str, int]

@config
class Config(BaseConfigModel):
    """Root configuration model."""

    bot: BotConfig
    constants: Constants
    logging: LoggingConfig = attr.ib(factory=LoggingConfig)
    database: PostgresConfig = attr.ib(factory=PostgresConfig)
    bot_manager: Optional[BotManagerConfig] = None
    welcomer: Optional[WelcomerConfig] = None
    assignable_roles: List[int] = attr.ib(factory=list)
    role_menus: List[RoleMenuConfig] = attr.ib(factory=list)
