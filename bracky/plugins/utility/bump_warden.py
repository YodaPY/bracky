#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of Bracky, a private guild bot for DCA
# Copyright (C) 2019-2020  Tmpod

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import asyncio
import typing

import hikari
import lightbulb

from bracky.config import get_config
from bracky.resources import error_responses
from bracky.utils import BrackyPlugin


class BumpWardenPlugin(BrackyPlugin):
    """
    Plugin to help in consistent bumping in server lists.
    It schedules reminders when a list is successfully bumped. At the moment it supports Disboard
    and Discord-Servers.com.
    """

    DISBOARD_BOT_ID: typing.Final[int] = 302050872383242240
    """Discord ID of Disboard's bot."""

    DISCORDSERVERS_BOT_ID: typing.Final[int] = 315926021457051650
    """Discord ID of Discord-Servers.com bot."""

    DISBOARD_BUMP_INTERVAL: typing.Final[float] = 2.0
    """Disboard bumping cooldown (in hours)."""

    DISCORDSERVERS_BUMP_INTERVAL: typing.Final[float] = 4.0
    """Discord-Servers.com bumping cooldown (in hours)."""

    DISBOARD_BUMP_BANNER_URL: typing.Final[
        str
    ] = "https://disboard.org/images/bot-command-image-bump.png"
    """URL of banner used in successful bump messagefor Disboard."""

    DISCORDSERVERS_BUMP_MESSAGE: typing.Final[str] = "Server bumped by"
    """Success message sent by Discord-Servers.com bot when bumping."""

    BUMP_REPLY_TIMEOUT: typing.Final[int] = 5
    """Maximum time (in minutes) to wait for a bots reply to a bump command."""

    def __init__(self, bot: lightbulb.Bot):
        # Monkey patch CommandNotFoundError action to ignore discord-servers.com captchas.
        error_responses.RESPONSES[lightbulb.errors.CommandNotFound] = self._patched_suggest_command

        super().__init__(bot)

    @staticmethod
    async def _patched_suggest_command(event: lightbulb.CommandErrorEvent) -> None:
        """
        Patched version of the suggest_command action, that checks if the command is a
        Discord-Servers.com CAPTCHA solution, before proceeding with the normal behaviour.
        """
        if not all(c.isdigit() for c in event.exception.args[0][:4]):
            await error_responses.suggest_command(event)

    def _make_reminder(self, ctx: lightbulb.Context, hours: float, board: str) -> None:
        """Fires up a task that reminds the context's author about the given board."""

        async def _send_reminder() -> None:
            await ctx.message.add_reaction("\N{ALARM CLOCK}")
            await asyncio.sleep(hours * 3600)
            await ctx.respond(
                (
                    ">>> \N{ALARM CLOCK} **Bump reminder**\n"
                    f"{ctx.author.mention} you can bump {board} again!"
                ),
                user_mentions=True,
            )

        asyncio.create_task(_send_reminder())

    async def _wait_for_reply(
        self,
        channel_id: int,
        user_id: int,
        condition: typing.Callable[[hikari.Message], bool],
    ) -> bool:
        """
        Waits for a message sent in the given channel and by the given user.
        I could have done this for each list implementation, but this way the common predicate is
        refactored out.
        """

        def predicate(event: hikari.MessageCreateEvent) -> bool:
            return (
                event.author_id == user_id
                and event.channel_id == channel_id
                and condition(event.message)
            )

        try:
            await self.bot.wait_for(
                hikari.MessageCreateEvent,
                predicate=predicate,
                timeout=self.BUMP_REPLY_TIMEOUT * 60,
            )
            return True
        except asyncio.TimeoutError:
            return False

    @lightbulb.command(name="bump")
    async def handle_discordservers_bump_command(self, ctx: lightbulb.Context) -> None:
        """
        Bumps the server on <https://discordservers.com>.
        This can be done every four hours.
        """

        def condition(message: hikari.Message) -> bool:
            # Explicit return of bool instead of Sequence[Embed] | bool
            return bool(
                message.embeds
                and message.embeds[0].description is not None
                and self.DISCORDSERVERS_BUMP_MESSAGE in message.embeds[0].description
            )

        ok = await self._wait_for_reply(ctx.channel_id, self.DISCORDSERVERS_BOT_ID, condition)
        if ok:
            self._make_reminder(
                ctx,
                self.DISCORDSERVERS_BUMP_INTERVAL,
                "Discord-Servers (`!bump`)",
            )

    @lightbulb.group(name="d")
    async def disboard_handler_group(self, _) -> None:
        """Bump the server on <https://disboard.org> with `d bump`"""

    @disboard_handler_group.command(name="bump")
    async def handle_disboard_bump_command(self, ctx: lightbulb.Context) -> None:
        """
        Bump the server on <https://disboard.org>.
        This can be done every two hours.
        """

        def condition(message: hikari.Message) -> bool:
            return bool(
                message.embeds
                and message.embeds[0].image is not None
                and message.embeds[0].image.url == self.DISBOARD_BUMP_BANNER_URL
            )

        ok = await self._wait_for_reply(ctx.channel_id, self.DISBOARD_BOT_ID, condition)
        if ok:
            self._make_reminder(ctx, self.DISBOARD_BUMP_INTERVAL, "Disboard (`!d bump`)")


def load(bot: lightbulb.Bot):
    bot.add_plugin(BumpWardenPlugin(bot))


def unload(bot: lightbulb.Bot):
    bot.remove_plugin("BumpWardenPlugin")
