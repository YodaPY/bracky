#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of Bracky, a private guild bot for DCA
# Copyright (C) 2019-2020  Tmpod

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import time
import typing

import aiohttp
import hikari
import lightbulb

from bracky.utils import BrackyPlugin


class PingPongPlugin(BrackyPlugin):
    """Pinging related commands."""

    PING_URL: typing.Final[str] = "https://discord.com/api/gateway"

    @staticmethod
    async def ping_http() -> float:
        async with aiohttp.ClientSession(raise_for_status=True) as session:
            start = time.perf_counter()
            async with session.head(PingPongPlugin.PING_URL):
                return time.perf_counter() - start

    @lightbulb.command(name="ping", aliases=("pong",))
    async def ping_command(self, ctx: lightbulb.Context):
        """
        Pings the bot and retrieves its latency.

        Pings the bot which will measure both the Discord Gateway heartbeat latency
        and the real time messages take to process (aka "ACK", from "acknowledgement")
        :ping_pong:
        """
        msg = await ctx.respond(">>> *Pinging...*")
        time_delta = (await PingPongPlugin.ping_http()) * 1000

        # the latency shouldn't be None when this is ever run
        heartbeat = ctx.bot.heartbeat_latency * 1000

        ping_or_pong = "Pong!" if ctx.invoked_with == "ping" else "Ping!"
        await msg.edit(
            content=(
                f">>> **{ping_or_pong}** :ping_pong:\n"
                f":heartbeat: **Heartbeat Latency:** `{heartbeat:,.2f}ms`\n"
                f":file_cabinet: **ACK Latency:** `{time_delta:,.2f}ms`"
            )
        )


def load(bot: lightbulb.Bot):
    bot.add_plugin(PingPongPlugin(bot))


def unload(bot: lightbulb.Bot):
    bot.remove_plugin("PingPongPlugin")
