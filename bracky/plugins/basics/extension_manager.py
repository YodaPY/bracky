#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of Bracky, a private guild bot for DCA
# Copyright (C) 2019-2020  Tmpod

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import lightbulb

from bracky.utils import bracky_dev_check, BrackyPlugin


class ExtensionManagerPlugin(BrackyPlugin):
    """Load, unload or reload extensions easily"""

    @bracky_dev_check()
    @lightbulb.group(
        name="plugins",
        aliases=("pl",),
        brief="Plugin management tools",
    )
    async def extensions_group(self, ctx: lightbulb.Context):
        """
        Assortment of useful tools to manage Bracky's extensions.
        Passing no subcommand will be the same as invoking the `list` subcommand.
        """
        # Should invoke the `list` subcommand.
        raise NotImplemented()

    @extensions_group.command(name="list", aliases=("ls",), brief="Lists some info on plugins")
    async def list_extensions(self, ctx: lightbulb.Context):
        """Lists all loaded, unloaded and available plugins."""
        raise NotImplemented()

    @extensions_group.command(name="load", aliases=("l",), brief="Loads the given plugin")
    async def load_extension(self, ctx: lightbulb.Context, plugin: str):
        """Attempts to the given plugin if it's not loaded already."""
        raise NotImplemented()

    @extensions_group.command(name="unload", aliases=("u",), brief="Unloads the given plugin")
    async def unload_extension(self, ctx: lightbulb.Context, plugin: str):
        """Unloads a loaded plugin."""
        raise NotImplemented()

    @extensions_group.command(name="reload", aliases=("r",), brief="Reloads the given plugin")
    async def reload_extension(self, ctx: lightbulb.Context, plugin: str):
        """
        Unloads then loads the given plugin, effectively reloading it.
        If it wasn't loaded before, it will just act as loading.
        """
        raise NotImplemented()


def setup(bot: lightbulb.Bot):
    bot.add_plugin(ExtensionManagerPlugin(bot))


def unload(bot: lightbulb.Bot):
    bot.remove_plugin("ExtensionManagerPlugin")
