#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# This is part of Bracky, a private guild bot for DCA
# Copyright (C) 2019-2020  Tmpod

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

__all__ = ["BotManagerController"]

import logging
import typing

import hikari.api.rest
import hikari.api.cache
import hikari.errors
import tortoise
from tortoise.query_utils import Q

from bracky.config import get_config
from bracky.database import Bot, MAX_PREFIX_LENGTH


_LOGGER: typing.Final[logging.Logger] = logging.getLogger("bracky.bots.manager")

# Black is being a bit weird here...
INVITE_MESSAGE_CONTENT_TEMPLATE: typing.Final[
    str
] = """
>>> \N{ROBOT FACE} **New bot invite request**
<@{owner_name}> has requested `{bot_name}` (ID: `{bot_id}`) to be added to the server.
Use the link below to invite it!
**<https://discord.com/oauth2/authorize?client_id={bot_id}&scope=bot&guild_id={guild_id}>**
"""

INVITE_TICK_EMOJI: typing.Final[str] = "\N{OK HAND SIGN}"

PREFIX_NICK_OPENING_TOKEN: typing.Final[str] = "["
PREFIX_NICK_CLOSING_TOKEN: typing.Final[str] = "] "

PREFIX_NICK_TEMPLATE: typing.Final[str] = PREFIX_NICK_OPENING_TOKEN + "{prefix}" + PREFIX_NICK_CLOSING_TOKEN + "{name}"

OWNER_NOTIF_MESSAGE: typing.Final[
    str
] = """
>>> \N{PARTY POPPER} **Bot successfully invited**
Congratulations, <@{bot_id}> has been added to DCA!
You can now use it in the spam channels to your heart's content \N{GRINNING FACE WITH SMILING EYES}

For the sake of convenience, all bots are nicked to include their registered prefixes. This way everyone can easily know
which prefix to use when playing around with a bot. In this case, your bot's nickname will start with `{nick_prefix}`.

Happy botting!
"""


# noinspection PyMethodMayBeStatic
class BotManagerController:
    """
    Little class that abstracts away the database and Discord API calls for the bot management system's core functionality.
    It provides a series of nice methods that serve as an interface for the system, and are used to simplify the logic
    of each command and listener in the actual plugin.
    """

    def __init__(self, rest: hikari.api.rest.RESTClient, cache: hikari.api.cache.Cache):
        self.config = get_config()
        self.rest = rest
        self.cache = cache

    # Not entirely sure if we really need the query by prefix thing, but it's unique and it doens't really hurt.
    # The main thing that worries me with this approach is the slight inconsistency in the interface, because some
    # methods required the actual bot user ID for REST calls, so you can't feed it the internal ID or prefix.
    # One solution to this would be to always fetch the bot record first, but I'm unsure that would be a good idea.
    def _make_query(self, target: typing.Union[int, str]) -> Q:
        """
        Returns a tortoise query object according to the target type.
        If it's a string it will try to match a prefix, else it will try to match to the internal ID or bot user ID.
        """
        return Q(prefix=target) if isinstance(target, str) else Q(id=target) | Q(bot_id=target)

    async def create_bot(self, bot_id: int, owner_id: int, prefix: str, invite_message_id: int) -> Bot:
        """Creates a record on the database for a new bot."""
        _LOGGER.debug(
            "Creating record for bot with ID %s, owner %s, prefix %s and invite message %s",
            bot_id,
            owner_id,
            prefix,
            invite_message_id,
        )
        # Let it fail with UniqueViolationError
        return await Bot.create(bot_id=bot_id, owner_id=owner_id, prefix=prefix, invite_message_id=invite_message_id)

    async def get_bot(self, target: typing.Union[int, str]) -> typing.Optional[Bot]:
        """Fetches the bot record that matches the given target. If it doesn't find any match, it will return None."""
        _LOGGER.debug("Fetching record for bot target %s", target)
        return await Bot.get_or_none(self._make_query(target))

    async def delete_bot(self, target: typing.Union[int, str]) -> None:
        """Deletes the bot record that matches the given target."""
        _LOGGER.debug("Deleting record for bot target %s", target)
        await Bot.filter(self._make_query(target)).delete()

    def get_bots_for_owner_iter(self, owner_id: int) -> tortoise.queryset.QuerySet[Bot]:
        """Returns a query for all bots that the given user owns."""
        return Bot.filter(owner_id=owner_id).all()

    async def get_bots_for_owner(self, owner_id: int) -> typing.List[Bot]:
        """Returns a list of bot records for all bots that the given user owns."""
        _LOGGER.debug("Fetching bots for owner %s", owner_id)
        return await self.get_bots_for_owner_iter(owner_id)

    async def create_invite(self, bot_id: int, owner_id: int, prefix: str) -> Bot:
        """
        Produces a nice invite message that notifies staff about a new bot invite request, including a link without
        permissions and with the target guild already set. A database record for the bot is also created.
        The invite message will later get a reaction when the bot is invite, so the team knows it's already been
        dealt with.
        """
        _LOGGER.debug("Creating invite request for bot %s", bot_id)
        bot_user = await self.rest.fetch_user(bot_id)
        message = await self.rest.create_message(
            channel=self.config.bot_manager.invite_log_channel,
            content=INVITE_MESSAGE_CONTENT_TEMPLATE.format(
                owner_name=owner_id,
                bot_name=f"{bot_user.username}#{bot_user.discriminator}",
                bot_id=bot_id,
                guild_id=self.config.constants.guild,
            ),
        )

        bot_record = await self.create_bot(bot_id, owner_id, prefix, message.id)
        return bot_record

    @staticmethod
    def check_prefix_length(prefix: str) -> bool:
        """Checks whether the given prefix abides the maximum length set."""
        return prefix <= MAX_PREFIX_LENGTH

    async def update_bot_nick_prefix(
        self, bot_id: int, new_prefix: str, *, username: typing.Optional[str] = None
    ) -> None:
        """
        Updates a bot's nickname to match its new prefix.
        The template can be found in the `PREFIX_NICK_TEMPLATE` constant.
        The username param can be passed to avoid hitting the API with a request.
        """
        if self.check_prefix_length(new_prefix):
            return

        _LOGGER.debug("Changing bot %s nick to new prefix", new_prefix)

        if username is None:
            username = (await self.rest.fetch_user(bot_id)).username

        await self.rest.edit_member(
            self.config.constants.guild,
            bot_id,
            nick=PREFIX_NICK_TEMPLATE.format(prefix=new_prefix, name=username),
        )

    async def update_bot_prefix(self, bot_id: int, new_prefix: str) -> None:
        """Updates the bot's database record with a new prefix."""
        if self.check_prefix_length(new_prefix):
            return

        _LOGGER.debug("Changing bot %s prefix to %s", bot_id, new_prefix)
        await Bot.filter(bot_id=bot_id).update(prefix=new_prefix)

    async def update_bot_owner(self, target: typing.Union[int, str], new_owner_id: int) -> None:
        """Updates the bot's database record with a new owner."""
        _LOGGER.debug("Changing owner of bot target %s to %s", target, new_owner_id)
        await Bot.filter(self._make_query(target)).update(owner_id=new_owner_id)

    async def kick_bot(self, bot_id: int, *, reason: str = "") -> None:
        """Kicks the given bot from the guild and deletes its database record."""
        if reason:
            reason = f"- {reason}"

        _LOGGER.debug("Kicking bot %s with reason %s", bot_id, reason)
        await self.rest.kick_member(self.config.constants.guild, bot_id, reason=f"Owner used the kick command {reason}")

    async def give_roles(self, bot_record: Bot) -> None:
        """Assigns the Bots role to the bot and the Bot Developers role to its owner."""
        _LOGGER.debug("Giving Bots role to bot %s", bot_record.bot_id)
        await self.rest.edit_member(
            self.config.constants.guild, bot_record.bot_id, roles={self.config.bot_manager.bots_role}
        )

        _LOGGER.debug("Giving Bot Developers role to user %s", bot_record.owner_id)
        bot_owner = await self.rest.fetch_member(self.config.constants.guild, bot_record.owner_id)
        await self.rest.edit_member(
            self.config.constants.guild,
            bot_owner.id,
            roles=set(bot_owner.role_ids) | {self.config.bot_manager.bot_developers_role},
        )

    async def notify_owner(self, bot_record: Bot) -> None:
        """Sends a DM to the bot's owner letting them know it has been invited to the guild."""
        user = self.cache.get_user(hikari.Snowflake(bot_record.owner_id))
        if user is None:
            _LOGGER.warning("Couldn't find user %s to notify about new bot", bot_record.owner_id)

        dm_channel = await user.fetch_dm_channel()
        await dm_channel.send(
            OWNER_NOTIF_MESSAGE.format(
                bot_id=bot_record.id,
                # remove the last space ew
                nick_prefix=PREFIX_NICK_TEMPLATE.format(prefix=bot_record.prefix, name="")[:-1],
            ),
        )

    async def tick_invite_message(self, bot_record: Bot) -> None:
        """Reacts to a bot's invite request message to mark it as done."""
        _LOGGER.debug("Ticking invite message %s", bot_record.invite_message_id)
        await self.rest.add_reaction(
            self.config.bot_manager.invite_channel, bot_record.invite_message_id, INVITE_TICK_EMOJI
        )

    @staticmethod
    def get_prefix_from_nick(nick: str) -> typing.Optional[str]:
        """
        Parses a given nickname and attempts to extract a bot prefix from it.
        Returns None if the username doesn't follow the format.
        """
        nick = nick.strip()
        if nick[0] != PREFIX_NICK_OPENING_TOKEN:
            return
        try:
            close_index = nick.index(PREFIX_NICK_CLOSING_TOKEN)
        except ValueError:
            return

        return nick[1:close_index]
